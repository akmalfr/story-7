from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import accordion

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'accordion.html')

class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        self.selenium.get('http://127.0.0.1:8000')

    def tearDown(self):
        self.selenium.quit()

    def test_correct_page(self):
        selenium = self.selenium
        time.sleep(5)

        self.assertIn("Accordion", selenium.page_source)

    