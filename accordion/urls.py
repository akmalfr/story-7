from django.urls import path
from accordion.views import accordion

app_name = 'accordion'

urlpatterns = [
    path('', accordion, name="accordion"),
]